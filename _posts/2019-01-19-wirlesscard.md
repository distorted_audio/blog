---
layout: default
title: Wireless Mag stripe
date: 2019-01-19 16:02:35
tags: electronics
app: notepad
---
In college the job I had required me to use an ID badge to swipe into a lot of different areas around campus. I grew annoyed having to take this ID badge out of my wallet so frequently, so I sought a better solution. The ID badge system my school used was a simple magnetic stripe system. This system consists of a card with a magnetized strip and a reader capable of reading the magnetic pattern of the card. Investigations into how to wirelessly interface with such a  system have already been done in the past; notable examples include famed hacker [Geohot using a coil of wire](https://www.youtube.com/watch?v=FSJ02-T2atc) attached to the headphone jack of his smartphone to interface with the readers around my campus, and Samsung found a way to accomplish this with the NFC antenna in their smartphones to enable users to make payments at terminals that do not support wireless payments. I also came across this open-sourced project, [magspoof](https://github.com/samyk/magspoof), that some of this work was based upon.

I made a circuit that would wirelessly transmit the information in the badge to the magnetic stripe readers. This design uses an msp430 microcontroller and is powered by a lipo battery. The PCB and c code can be found in this git repository. [Wirless Card](https://gitlab.com/distorted_audio/wireless-card)

![Wirless Mag Stripe PCB](/assets/posts/img/wireless-pcb.png)
